# We want (redhat)/ centos
# we want httpd

# base for our container
FROM centos:centos8.3.2011

#run allows you to run command (bash)
  # if it was debian: apt-get -y install apache2
RUN yum -y install httpd 
## the services and apps you install is what determines what is running inside
## and what port are being used.
# If you knoe your service is going to be using port xyz
# then you can use EXPOSE to map and point to that port
# this Important so other services know where this service communicates
EXPOSE 80
#EXPOSE 443
# but you can manually map them using -p 80:something

# Syntax
# COPY localr_dir /container_app_dir
# COPY index.html /var/www/html
## Provisioning - sending code and setting up stuff
## I want to send this html into my container! 
# Syntax
# COPY localr_dir /container_app_dir
# COPY index.html /var/www/html
COPY index.html /var/www/html




# starting the service when container starts
# two option CMD & ENTRYPOINT
# ENTRYPOINT is the best practice
ENTRYPOINT ["httpd", "-DFOREGROUND"]
# like user data but easier. 
