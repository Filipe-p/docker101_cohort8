# Docker 101 :taco:

Intro about the class and the repo.

The repo will include information about git branching.

## Docker and containarization

Docker is a containarization


## Git Branching

A way to manage your code in git and bitbucket where you can create parallel timelines from specific point in your code timeline.

This is useful to developers to devloper specific features and not break the `main/master` branch where only *WORKING CODE* resides.

It is useful for DevOps engineers, as we build the workflow that developer use, and can assign actions to different branches using the section of `Jenkins Triggers` and defining what branches to listen to.

**Example of Devops CI Work Flow**

1. Master/main exists in Bitbucket
2. Devolpers make sub branches that that with *dev-* - so something link `git checkout -b dev-<FEATURE_NAME>` 
3. Jenkins is listining to branch with `dev-*`  - The asterix is a wild card.
4. Jenkins pull Dev-*, runs tests 
5. If tests pass, it attempts to merget to `master/main`

Hence, developers do not touch `main/master`, only jenkins after running tests, unsuring working code.

### Git Branching commands 

```bash

# creating NEW branch
$ git checkout -b <branch_name>

# Pushing to remote_repo from branch
$ git push <remo<te_repo> <local_branch>
$ git push origin dev-branching

# Go merge the pull request on bitbuck
## make a pull request
## merge pull request

# update local machine with new master
## but first checkout to mast
$ git checkout master
$ git pull origin master

## Repeat and make new branches

## changing branches
$ git checkout <branch>
```

**note on committing** your changes follow you until you commit them. 

**other behaviors** you can protect master branch from users and only a "jenkins" user or aproved user can merge to master / push to master.

Workflows are different in every company and evolve slowly.


## Docker

Docker is a container technology. 
It has images, and you build containers.


### Main commands

```

# docker run

docker run -d -p 80:80 docker/getting-started

## -d is detached mode - so it doesn't stay running in the terminal
## -p is port - external_port:internal_port 

# going into a container to debug or look around
docker exec -it <container_name> /bin/bash 

## runing simple one line commands on a container:
docker exec <container_name> ls

## 

docker kill <container_id>
docker stop <container_id>

```

### Lets setup an DB and learn about -e and volumes!

### Sending environment variables with docker

```

docker run --name book_sql -e MYSQL_ROOT_PASSWORD=mega-secret

```


### Volumes 

Volumes is like a syched folder for containers.

when you run the container you can specicy -v.

```bash

## Syntax

docker run -v local_dir:container_dir 

docker run -d --name book_sql -e MYSQL_ROOT_PASSWORD=mega-secret -v /Users/filipepaiva/code/cohor8-AL/docker101_cohort8/volume_mysql:/var/lib/mysql mysql

docker exec -it book_sql mysql -u root -pmega-secret


```